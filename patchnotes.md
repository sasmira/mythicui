# Patchnotes
## Compatibility
- Ready for v11 version.


## v11.306.1
- First release for Foundry VTT v11

## v10.284.1
- First release for Foundry VTT v10
## v9.255.1
- Fix Normal/Yes/Default button does not have the Marble texture. thx @Brimcon

## v9.249.1
- Sidebar Macro Icon support added

## v9.245.5
- Fix Combat Carousel texture portrait
- Fix Combat Carousel controls icon 

## v9.245.4
- Add custom dice 
- Clean code

## v9.245.3
- Combat Carousel compatibility
- New logo
- Improved visual

## v9.245.1
- First commit