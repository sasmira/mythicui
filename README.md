# Mythic UI Rebirth
## An Odyssey of the Dragonlords flavored UI for Foundry VTT.
- Fork of [MythicUI by Sparkasaurusmex]( https://github.com/Sparkasaurusmex/OotDui) was originally based on Fantasy UI by iotech
- Updated for Foundry VTT v9

## Installation

To install, follow these instructions:

1.  Inside Foundry, select the Game Modules tab in the Configuration and Setup menu.
2.  Click the Install Module button and enter the following URL: 
https://gitlab.com/sasmira/mythicui/-/raw/main/module/module.json
3.  Click Install and wait for installation to complete.

## Mythic UI Rebirth, it's :
![OotDuiExample.png](OotDuiExample.png)

<img src="https://puu.sh/IG8Ah/3734ba4589.jpg">

- Set of the Dice and Texture of [Nice more Dice](https://github.com/LyncsCwtsh/fvtt-module-Nice-more-Dice) by LyncsCwtsh
<img src="https://puu.sh/IGc44/bacaf47f04.jpg">
